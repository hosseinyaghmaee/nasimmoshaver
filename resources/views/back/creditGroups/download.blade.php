<table >
    <thead>
    <tr>
        <th>Code</th>
    </tr>
    </thead>
    <tbody>
    @foreach($creditGroup->credits as $credit)
        <tr>
            <td>{{ $credit->identifier }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
