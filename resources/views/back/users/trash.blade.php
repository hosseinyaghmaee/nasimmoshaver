@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.dashboard.index"),"Users" =>route("admin.users.index"),"current"=>"آرشیو کاربران"]])
    @endcomponent
@endsection

@section("content")
    <x-success></x-success>
    <div class="card">
        <div class="card-body">

            {{ $users->render() }}
            <table class="table">
                <thead>
                <tr>
                    <th></th>
                    <th>نام</th>
                    <th>تعداد نماد</th>
                    <th>شماره موبایل</th>
                    <th>تاریخ شروع پشتیبانی</th>
                    <th>تاریخ اتمام</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td></td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->symbols_count }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>{{ $user->support_start_fa_f }}</td>
                        <td>{{ $user->support_end_fa_f }}</td>
                        <td><a href="{{ route("admin.users.recycle",$user) }}">خروج از آرشیو</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
