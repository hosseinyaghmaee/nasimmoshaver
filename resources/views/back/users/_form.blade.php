<form action="{{ route('admin.users.store')  }}"
      method="post" enctype="multipart/form-data">
    @csrf

    {{ Html::hidden()->name('user_id')->value($user->id)->when( $action=='edit') }}
    {{ Html::method()->value("PUT")->when($action=='edit') }}

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{
                    Html::text("name")
                    ->value(old("name",$user->name))
                    ->label("نام")
                    ->description("نام و نام خانوادگی را وارد کنید")
                     }}
                    {{
                    Html::text("simotel_number")
                    ->value(old("simotel_number",$user->simotel_number))
                    ->label("شماره داخلی سیموتل")
                     }}
                    {{
                    Html::text("email")
                    ->value(old("email",$user->email))
                    ->label("ایمیل")
                     }}
                    {{ Html::text("mobile")
                        ->value(old("mobile",$user->mobile))
                        ->label("شماره موبایل")
                        ->description("مثل: 09370311680")
                    }}
                    {{ Html::text("address")
                        ->value(old("address",$user->address))
                        ->label("آدرس")
                    }}

                    {{
                    Html::text("credit_ratio")
                    ->value(old("credit_ratio",$user->credit_ratio))
                    ->label("ضریب کسر اعتبار")
                    ->description("کسر هزینه(تومان) به ازای هر دقیقه مکالمه")
                     }}
                    {{
                    Html::text("entrance_fee")
                    ->value(old("entrance_fee",$user->entrance_fee)  ?? 0)
                    ->label("هزینه ورودی")
                    ->description("به تومان")
                     }}
                    {{ Html::text("comment")
                        ->value(old("comment",$user->comment))
                        ->label("توضیحات")
                        ->description("")
                    }}
                    {{ Html::select("level")
                        ->select_options([\App\User::LEVEL_ADMIN=>"ادمین",\App\User::LEVEL_ADVISOR=>"مشاور"])
                        ->label("توضیحات")
                        ->value(old("level",$user->level))
                        ->description("")
                    }}

                    <hr>


                    <div class="form-group ">
                        <label for="password" class=" col-form-label">رمز عبور</label>

                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror" name="password"
                               autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror

                        <small>فقط در صورتی که میخواهید رمز کاربر را بروزرسانی کنید</small>
                    </div>
                    <div class="form-group ">
                        <label for="password-confirm" class="">تایید رمز عبور</label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                               autocomplete="new-password">
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">{{ Html::submit()->label('ذخیره') }}</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>
