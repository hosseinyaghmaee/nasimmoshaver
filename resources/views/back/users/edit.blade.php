@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",
    ["items"=> ["Dashboard" =>route("admin.users.index"),"Users" =>route("admin.users.index"),"current"=>"ویرایش اطلاعات کاربر"]])
    @endcomponent
@endsection

@section("content")
    <x-success></x-success>
        @include("back.users._form",["action"=>"edit"])
@endsection
