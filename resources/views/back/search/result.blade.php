@extends("back.layout.app")
@section("content_header")
    @component("components.breadcrump",["items"=> ["Dashboard" =>route("admin.dashboard.index"),"current"=>"جستجو"]])
    @endcomponent
@endsection

@section("content")

    <div class="card">
        <div class="card-body">
            <strong>نتایج جستجو برای</strong>:
            <span class="text-danger">"{{ request()->q }}"</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-users"></i>
                    نتایج جستجو در فهرست کاربران
                </div>
                <div class="card-body">
                    @if($users->count() == 0)
                        هیچ کاربری یافت نشد
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>نام</th>
                                <th>شماره موبایل</th>
                                <th>شماره سیموتل</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td></td>
                                    <td><a href="{{ route("admin.users.show",$user) }}">{{ $user->name }}</a></td>
                                    <td>{{ $user->mobile }}</td>
                                    <td>{{ $user->simotel_number }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-users"></i>
                    نتایج جستجو در فهرست صف ها
                </div>
                <div class="card-body">
                    @if($users->count() == 0)
                        هیچ صفی یافت نشد
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>نام</th>
                                <th>شماره سیموتل</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($queues as $queue)
                                <tr>
                                    <td><a href="{{ route("admin.queues.show",$queue) }}">{{ $queue->name }}</a></td>
                                    <td>{{ $queue->simotel_number }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-users"></i>
                    نتایج جستجو در فهرست کارت های اعتباری
                </div>
                <div class="card-body">
                    @if($users->count() == 0)
                        هیچ کارتی یافت نشد
                    @else
                        <table class="table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>شناسه</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($credits as $credit)
                                <tr>
                                    <td><a href="{{ route("admin.credits.show",$credit) }}">{{ $credit->identifier }}</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection
