@extends('layouts.app')

@section('content')

    <div class="home-page">
        <div class="container purchase-form mb-5 mt-3">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card ">
                        <div class="card-header bg-info text-white"><h2 class="h5">اطلاعات کارت اعتباری شما</h2>
                        </div>
                        <div class="card-body">


                            <div class="row">
                                <div class="col-md-12">
                                    {{ Html::info()->value($credit->identifier)->label("شناسه (پین کد)")->icon("key") }}
                                    {{ Html::info()->value(number_format($credit->remain_credit) . " تومان")->label("مانده اعتبار")->icon("coins") }}
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{ route("front.credits.updatePhoneNumber") }}"
                                          method="post">
                                        {{ Html::hidden("credit_unique_id")->value($credit->unique_id) }}
                                        @csrf
                                        {{
                                Html::text("customer_phone_number")
                                ->label("تغییر شماره تماس ورودی:")
                                ->value($credit->phone_number)
                                ->description("لطفا شماره مورد نظر خود را به دقت و به صورت کامل وارد کنید. مثل: 09150000000 یا 02133215252")
                                }}

                                        <input type="submit" value=" ثبت" class="btn btn-outline-info float-left">
                                    </form>
                                </div>
                            </div>

                            <x-errors></x-errors>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="{{ route("front.purchase") }}" method="post">
                                        @csrf
                                        {{ Html::hidden("credit_unique_id")->value($credit->unique_id) }}
                                        @php
                                        $plans = Options::group("site-options")->get("credit-plans","[]");
                                        $plans = explode(",",$plans);
                                        foreach ($plans as $value) {
                                            $plansArray[$value] = number_format((int)$value) . "تومان";
                                        }
                                        @endphp
                                        {{
                                           Html::select("amount")
                                           ->label("افزایش اعتبار کارت:")
                                           ->value("50000")
                                           ->description("")
                                           ->select_options($plansArray)
                                         }}
                                        <input type="submit" value="پرداخت" class="btn btn-outline-info float-left">
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
