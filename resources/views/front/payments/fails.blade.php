@extends('layouts.app')

@section('content')

    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card ">
                    <div class="card-header bg-info text-white"><h2 class="h5">اطلاعات پرداخت</h2>
                    </div>
                    <div class="card-body">

                        <div class="alert alert-danger">
                            متاسفانه، پرداخت موفقیت آمیز نبود.
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
