@php
    use Hsy\Html\Facades\Html;
@endphp
<x-success></x-success>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">افزودن برنامه حضور</div>
            <div class="card-body">
                @php
                    $schedule = new \App\Models\Schedule();
                    $user_id = $user->id ;
                @endphp
                @include("partials.schedules._form",['action'=>"create"])
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">برنامه حضور</div>
            <div class="card-body">
                @php
                    $overlappedIds=[];
                        if($errors->has("conflict")){
                            $overlappedIds = $errors->get("conflict")[0];
                            $overlappedIds = explode(",",$overlappedIds);
                        }

                @endphp

                <table class="table ">
                    <thead>
                    <tr>
                        <th>روز هفته</th>
                        <th>شروع</th>
                        <th>پایان</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->schedules as $schedule)
                        <tr @if(in_array($schedule->id,$overlappedIds)) class="text-white bg-warning" @endif >
                            <td>{{ $schedule->day_readable }}</td>
                            <td>{{ $schedule->start_time_formatted }}</td>
                            <td>{{ $schedule->end_time_formatted }}</td>
                            <td>
                                <form action="{{ route("advisor.schedules.remove",$schedule) }}" method="post">
                                    @csrf
                                    @method("DELETE")
                                    <button type="submit" class="btn btn-danger btn-sm">حذف</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


            </div>
        </div>
    </div>
</div>
