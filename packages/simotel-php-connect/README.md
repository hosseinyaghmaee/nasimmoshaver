# simotel-php-connect
[![Build Status](https://travis-ci.com/hsyir/simotel-php-connect.svg?branch=master)](https://travis-ci.com/hsyir/simotel-php-connect)
[![StyleCI](https://github.styleci.io/repos/325189235/shield?branch=master)](https://github.styleci.io/repos/325189235?branch=master)
> ### Simotel and Laravel
> if you want to connect to simotel with laravel please visit our  laravel package:
> [nasimtelecom/simotel-laravel-connect](https://github.com/nasimtelecom/simotel-laravel-connect)

Keep connected with Simotel by PHP. Simotel is a wonderful call center software with huge abilities.
visit Simotel documents here: [doc.mysup.ir](https://doc.mysup.ir/)

with this package you can easly connect to simotel server by php and do somethings amazing.



- [Install](#install)
- [Simotel Api](#simotel-api)
- [Simotel Event Api](#simotel-event-api)
- [Smart Api](#smart-api)

## Install

Use composer to install and autoload the package
```
composer require nasimtelecom/simotel-php-connect
```

## Simotel Api
Simotel Api help you to connect to simotel server remotely and manage simotel users, queues, trunks, announcements, get reports, send fax and all you need to do with Simotel call center.



#### simotel api connect

```php
use \Hsy\Simotel\Simotel;
use \Hsy\Simotel\SimotelApi\Parameters;

$config = Simotel::getDefaultConfig();
$config["simotelApi"]["connect"]=[
    'user'=> 'apiUser',
    'pass'=> 'apiPass',
    'token'=> 'apiToken',
    'server_address' => 'http://simotelServer',
                                 
];

$simotel = new Simotel($config);

$parameters = new Parameters();
$parameters->name = "user name";
$parameters->number = "101";
$parameters->secret = "password";
  
/**  
 * result is instance of \Psr\Http\Message\ResponseInterface  
 */
$result = $simotel->connect()->pbx()->users()->add($parameters);
```



## Simotel Event Api



1. Create listeners for events

```php
$simotel = new Simotel();
$simotel->eventApi()->addListener('Cdr', function ($simotelApiData) {
    var_dump($simotelApiData);
});
```

2. Dispatch events after receive request from simotel event api on your api endpoint
> you must put your api endpoint address on Simotel setting

```php
use \Hsy\Simotel\Simotel;
$simotelEventApiData =  $_POST["api_data"];
$eventName = $_POST["api_data"]["event_name"];
$simotel = new Simotel();
$simotel->eventApi()->dispatch($eventName,$simotelEventApiData);
```

## Simotel Smart Api

1. create smartApp classes and methods that called by smart api apps

**Don't forget to import SmartApiCommands trait**

```php

use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;


class PlayWelcomeMessage
{
    use SmartApiCommands;
    
    public function playAnnounceApp($appData)
    {
        $this->cmdPlayAnnouncement("announcement file name");
        return $this->okResponse();
    }
}

class RestOfApps
{
    use SmartApiCommands;
    
    public function sayClock($appData)
    {
        $this->cmdSayClock("22:00");

        return $this->okResponse();

    }

    public function interactiveApp($appData)
    {
        if($appData["data"]=="1")
            return $this->okResponse();

        if($appData["data"]=="2")
            return $this->errorResponse();
            
    }
}

```


there is all commands you can use in smartApp class:

```php
cmdPlayAnnouncement($file);
cmdPlayback($file);
cmdExit($exit);
cmdGetData($file,$timeout,$digitsCount);
cmdSayDigit($number);
cmdSayNumber($number);
cmdSayClock($clock);
cmdSayDate($date,$calender);
cmdSayDuration($duration);
cmdSetExten($exten);
cmdSetLimitOnCall($seconds);
cmdMusicOnHold();
```

2. handle received request from simotel smart api

```php
$config = Simotel::getDefaultConfig();
$config["smartApi"]["appClasses"] = [
  'playWelcomeMessage' => PlayWelcomeMessage::class,
  '*' => RestOfApps::class,
];

// place this codes where you want grab income requests
// from simotel smartApi calls     
$simotel = new Simotel($config);
$appData = $_POST["app_data"];
$jsonResponse = $simotel->smartApiCall($appData)->toJson();
$arrayResponse = $simotel->smartApiCall($appData)->toArray();

echo $jsonResponse;
/*
 if app_name='playAnnounceApp' 
	 response = {'ok':1,'commands':'PlayAnnouncement('announcement file name')'}

 if app_name='sayClock' 
	 response = {'ok':1,'commands':'SayClock("14:00")'}

 if app_name='interactiveApp' 
	 if data=1 
		 response = {'ok':1}
	 if data=2 
		 response = {'ok':0}
*/
```
