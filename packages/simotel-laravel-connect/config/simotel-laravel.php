<?php

return [
    'smartApi' => [
        'apps' => [
            'registerCall' => \App\Classes\Simotel\SmartApiApps\RegisterCall::class,
            'checkPincode' => \App\Classes\Simotel\SmartApiApps\CheckPincode::class,
            'selectAdvisor' => \App\Classes\Simotel\SmartApiApps\SelectAdvisor::class,
            'selectQueue' => \App\Classes\Simotel\SmartApiApps\SelectQueue::class,
            'registerPollScore' => \App\Classes\Simotel\SmartApiApps\RegisterPollScore::class,
            '*' => \App\Classes\Simotel\SmartApiApps\SmartApiMethodsRepo::class,
        ],
    ],
    'ivrApi' => [
        'apps' => [
            '*' => "\App\Simotel\IvrApiApp",
        ],
    ],
    'trunkApi' => [
        'apps' => [
            '*' => "\App\Simotel\TrunkApiApp",
        ],
    ],
    'extensionApi' => [
        'apps' => [
            '*' => "\App\Simotel\ExtensionApiApp",
        ],
    ],
    'simotelApi' => [
        'connect' => [
            'user' => env("SIMOTEL_API_USER"),
            'pass' => env("SIMOTEL_API_PASS"),
            'token' => 'apiToken',
            'server_address' => env("SIMOTEL_API_SERVER"),
        ],
        'methods' => [
            'pbx_users_add' => [
                'address' => 'pbx/users',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_users_update' => [
                'address' => 'pbx/users',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_users_remove' => [
                'address' => 'pbx/users',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_users_search' => [
                'address' => 'pbx/users',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_trunks_add' => [
                'address' => 'pbx/trunks',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_trunks_update' => [
                'address' => 'pbx/trunks',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_trunks_remove' => [
                'address' => 'pbx/trunks',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_trunks_search' => [
                'address' => 'pbx/trunks',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_queues_add' => [
                'address' => 'pbx/queues',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_queues_update' => [
                'address' => 'pbx/queues',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_remove' => [
                'address' => 'pbx/queues',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_queues_search' => [
                'address' => 'pbx/queues',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_queues_addagent' => [
                'address' => 'pbx/queues/addagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_removeagent' => [
                'address' => 'pbx/queues/removeagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_pauseagent' => [
                'address' => 'pbx/queues/pauseagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_resumeagent' => [
                'address' => 'pbx/queues/resumeagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_batchaddagent' => [
                'address' => 'pbx/queues/batchaddagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_batchremoveagent' => [
                'address' => 'pbx/queues/batchremoveagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_batchpauseagent' => [
                'address' => 'pbx/queues/batchpauseagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_queues_batchresumeagent' => [
                'address' => 'pbx/queues/batchresumeagent',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_blacklists_add' => [
                'address' => 'pbx/blacklists',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_blacklists_update' => [
                'address' => 'pbx/blacklists',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_blacklists_remove' => [
                'address' => 'pbx/blacklists',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_blacklists_search' => [
                'address' => 'pbx/blacklists',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_whitelists_add' => [
                'address' => 'pbx/whitelists',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_whitelists_update' => [
                'address' => 'pbx/whitelists',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_whitelists_remove' => [
                'address' => 'pbx/whitelists',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_whitelists_search' => [
                'address' => 'pbx/whitelists',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_announcements_upload' => [
                'address' => 'pbx/announcements/upload',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_announcements_add' => [
                'address' => 'pbx/announcements',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_announcements_update' => [
                'address' => 'pbx/announcements',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'pbx_announcements_remove' => [
                'address' => 'pbx/announcements',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'pbx_announcements_search' => [
                'address' => 'pbx/announcements',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_musiconholds_search' => [
                'address' => 'pbx/musiconholds',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_faxes_upload' => [
                'address' => 'pbx/faxes/upload',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_faxes_add' => [
                'address' => 'pbx/faxes',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'pbx_faxes_search' => [
                'address' => 'pbx/faxes',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'pbx_faxes_download' => [
                'address' => 'pbx/faxes/download',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'call_originate_act' => [
                'address' => 'call/originate/act',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'voicemails_voicemails_add' => [
                'address' => 'voicemails/voicemails',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'voicemails_voicemails_update' => [
                'address' => 'voicemails/voicemails',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'voicemails_voicemails_remove' => [
                'address' => 'voicemails/voicemails',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'voicemails_voicemails_search' => [
                'address' => 'voicemails/voicemails',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'voicemails_audio_download' => [
                'address' => 'voicemails/audio/download',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'voicemails_inbox_search' => [
                'address' => 'voicemails/inbox',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_quick_search' => [
                'address' => 'reports/quick',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_quick_info' => [
                'address' => 'reports/quick/info',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_audio_download' => [
                'address' => 'reports/audio/download',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_cdr_search' => [
                'address' => 'reports/cdr',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_cdr_ordered_id' => [
                'address' => 'reports/cdr/ordered/id',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_queue_search' => [
                'address' => 'reports/queue',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_queue_ordered_id' => [
                'address' => 'reports/queue/ordered/id',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_queue_details_search' => [
                'address' => 'reports/queue_details',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_queue_details_ordered_id' => [
                'address' => 'reports/queue_details/ordered/id',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_agent_search' => [
                'address' => 'reports/agent',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'reports_poll_search' => [
                'address' => 'reports/poll',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_announcements_upload' => [
                'address' => 'autodialer/announcements/upload',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_announcements_add' => [
                'address' => 'autodialer/announcements',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_announcements_update' => [
                'address' => 'autodialer/announcements',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'autodialer_announcements_remove' => [
                'address' => 'autodialer/announcements',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'autodialer_announcements_search' => [
                'address' => 'autodialer/announcements',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_campaigns_add' => [
                'address' => 'autodialer/campaigns',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_campaigns_update' => [
                'address' => 'autodialer/campaigns',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'autodialer_campaigns_remove' => [
                'address' => 'autodialer/campaigns',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'autodialer_campaigns_search' => [
                'address' => 'autodialer/campaigns',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_contacts_add' => [
                'address' => 'autodialer/contacts',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_contacts_update' => [
                'address' => 'autodialer/contacts',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'autodialer_contacts_remove' => [
                'address' => 'autodialer/contacts',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'autodialer_contacts_search' => [
                'address' => 'autodialer/contacts',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_groups_upload' => [
                'address' => 'autodialer/groups/upload',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_groups_add' => [
                'address' => 'autodialer/groups',
                'request_method' => 'POST',
                'default_request_data' => [],
            ],
            'autodialer_groups_update' => [
                'address' => 'autodialer/groups',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'autodialer_groups_remove' => [
                'address' => 'autodialer/groups',
                'request_method' => 'DELETE',
                'default_request_data' => [],
            ],
            'autodialer_groups_search' => [
                'address' => 'autodialer/groups',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_reports_search' => [
                'address' => 'autodialer/reports',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_reports_ordered_id' => [
                'address' => 'autodialer/reports/ordered/id',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_reports_info' => [
                'address' => 'autodialer/reports/info',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
            'autodialer_trunks_update' => [
                'address' => 'autodialer/trunks',
                'request_method' => 'PUT',
                'default_request_data' => [],
            ],
            'autodialer_trunks_search' => [
                'address' => 'autodialer/trunks',
                'request_method' => 'GET',
                'default_request_data' => [],
            ],
        ]
    ],
];
