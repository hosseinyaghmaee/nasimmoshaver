<?php

namespace Tests\Feature;

use App\Classes\Reports\CallsReports;
use App\Models\Call;
use App\Models\Credit;
use App\Models\Queue;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Services\CallSimolator;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function srcHasNoCredit()
    {
        $simulator = new CallSimolator($this);
        $this->createUser($simulator->exten);
        $wrongNumber= "09321321321";
        $this->createCredit(100000, $wrongNumber);
        
        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("noCredit", $response);
    }

    /**
     * @test
     */
    public function srcHasCredit()
    {
        $simulator = new CallSimolator($this);
        $this->createUser($simulator->exten);
        $this->createCredit(100000, $simulator->src);
        
        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("hasCredit", $response);
    }

    /**
     * @test
     */
    public function checkPincode()
    {
        $simulator = new CallSimolator($this);
        $credit = $this->createCredit(100000);
        $pincode =  $credit->identifier ;
        $wrongPincode =  $credit->identifier . "_wrong";
        
        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("noCredit", $response);

        //no pincode data passed to smartapi
        $response = $simulator->smartApiCall("checkPincode");
        $this->assertExitOn("wrongPincode", $response);

        //wrong pincode passed
        $response = $simulator->smartApiCall("checkPincode", $wrongPincode);
        $this->assertExitOn("wrongPincode", $response);

        //pincode passed
        $response = $simulator->smartApiCall("checkPincode", $pincode);
        $this->assertExitOn("hasCredit", $response);
    }
    
  
    /**
     * @test
     */
    public function selectWrongAdvisor()
    {
        $simulator = new CallSimolator($this);
        $this->createUser($simulator->exten);
        $this->createCredit(100000, $simulator->src);
        
        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("hasCredit", $response);

        $response = $simulator->smartApiCall("selectAdvisor");
        $this->assertExitOn("wrongExten", $response);
        
        $response = $simulator->smartApiCall("selectAdvisor", "101");
        $this->assertExitOn("wrongExten", $response);
    }

    /**
    * @test
    */
    public function selectAdvisor()
    {
        $simulator = new CallSimolator($this);
        
        $entranceFee = 1000;
        $creditRatio = 1000;
        $billSec = 60*5;
        $creditCharge = 5000;

        $user = $this->createUser($simulator->exten, $entranceFee, $creditRatio);
        $credit = $this->createCredit($creditCharge, $simulator->src);

        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("hasCredit", $response);

        $response = $simulator->smartApiCall("selectAdvisor", "101");
        $this->assertExitOn("wrongExten", $response);

        $response = $simulator->smartApiCall("selectAdvisor", "100");
        $this->assertExitOn("toExten", $response);

        $wrongScore= 8;
        $response= $simulator->smartApiCall("registerPollScore", $wrongScore);
        $this->assertExitOn("wrongScore", $response);

        $score= 4;
        $response= $simulator->smartApiCall("registerPollScore", $score);
        $this->assertExitOn("scoreRegistered", $response);

        $simulator->cdrSeaCall($billSec);
        $callCost = (($billSec/60) * $creditRatio) + $entranceFee;
        $this->assertEquals($creditCharge - $callCost, $credit->remain_credit);

        $report= new CallsReports;
        $report = $report->statisitcs(null, $user->id);
        $this->assertEquals($billSec, $report['call_time']);
        $this->assertEquals($score, (integer) $report['poll_score_avg']);
    }


    /**
     * @test
     */
    public function selectQueue()
    {
        $entranceFee = 1000;
        $creditRatio = 1000;
        $billSec = 60*5;
        $creditCharge = 10000;

        $simulator = new CallSimolator($this);

        $credit = $this->createCredit($creditCharge, $simulator->src);

        $user = $this->createUser($simulator->exten, $entranceFee, $creditRatio);

        $queue = $this->createQueue($simulator->exten, $entranceFee, $creditRatio);
        
        $response = $simulator->smartApiCall("registerCall");
        $this->assertExitOn("hasCredit", $response);

        $response = $simulator->smartApiCall("selectQueue");
        $this->assertExitOn("wrongQueue", $response);
        
        $response = $simulator->smartApiCall("selectQueue", "101");
        $this->assertExitOn("wrongQueue", $response);

        $response = $simulator->smartApiCall("selectQueue", $queue->announcement_id);
        $this->assertExitOn("toQueue", $response);
        
        $response= $simulator->smartApiCall("registerPollScore");
        $this->assertExitOn("wrongScore", $response);

        $wrongScore= 8;
        $response= $simulator->smartApiCall("registerPollScore", $wrongScore);
        $this->assertExitOn("wrongScore", $response);

        $score= 4;
        $response= $simulator->smartApiCall("registerPollScore", $score);
        $this->assertExitOn("scoreRegistered", $response);
        
        $simulator->cdrSeaCall($billSec);

        $callCost = (($billSec/60) * $creditRatio) + $entranceFee;
        $this->assertEquals($creditCharge - $callCost, $credit->remain_credit);

        $report= new CallsReports;
        $report = $report->statisitcs($queue->id);
        $this->assertEquals($billSec, $report['call_time']);

        $call=Call::first();
        $this->assertEquals($call->user_id, $user->id);
    }


    private function createCredit($charge=50000, $phoneNumber="")
    {
        $credit = new Credit;
        $credit->credit_group_id = 1;
        $credit->identifier =random_int(1, 50000);
        $credit->phone_number = $phoneNumber;
        $credit->save();
        $credit->chargeCredit($charge);
        return $credit;
    }

    private function createUser($simoteNumber, $entranceFee=1000, $creditRatio=1000)
    {
        $user = [
                'name' => 'advisor-1',
                'email' => 'advisor@demo.com',
                'password' => bcrypt("123456"),
                'simotel_number' => $simoteNumber,
                'mobile' => "09370000000",
                'comment' => "",
                'address' => "",
                'level' => User::LEVEL_ADMIN,
                'entrance_fee' => $entranceFee,
                'credit_ratio' => $creditRatio,
        ];

        return User::create($user);
    }
    
    private function createQueue($simoteNumber, $entranceFee=1000, $creditRatio=1000)
    {
        $queue = new Queue;
        $queue->name= 'queue-1';
        $queue->simotel_number = $simoteNumber;
        $queue->entrance_fee = $entranceFee;
        $queue->announcement_id = random_int(1, 999999);
        $queue->credit_ratio = $creditRatio;
        $queue->save();
        return $queue;
    }

    public function assertExitOn($route, $response)
    {
        $this->assertTrue(
            $this->checkExitPath($route, $response),
            "Failed on route '$route' - "
            . PHP_EOL . ($response->json()["commands"] ?? "")
        );
    }

    public function checkExitPath($route, $response)
    {
        $response = $response->json();
        if (!isset($response["commands"])) {
            return false;
        }

        return !(strpos($response["commands"], "Exit('$route')") === false);
    }
}
