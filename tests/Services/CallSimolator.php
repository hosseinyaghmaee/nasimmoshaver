<?php

namespace Tests\Services;

use Illuminate\Support\Facades\Http;

class CallSimolator
{
    public $src;
    public $dst;
    public $exten;
    private $uid;

    private $testClass;

    public function __construct($testClass)
    {
        $this->uid = random_int(100000, 99999999);
        $this->src="09151119999";
        $this->dst="100";
        $this->exten = "100";
        $this->testClass = $testClass;
    }

    public function smartApiCall($appName, $data=null)
    {
        $params = [
            "app_name"=>$appName,
            "src"=>$this->src,
            "dst"=>$this->dst,
            "data"=>$data,
            "unique_id"=>$this->uid,
        ];
        return $this->testClass->json('GET', '/api/smartApi', $params);
    }


    public function cdrSeaCall($billSec=60)
    {
        $cdrParams = [
            "event_name" => "Cdr",
            "starttime" => "2022-02-22 08:26:50.727587",
            "endtime" => "2022-02-22 08:27:17.727587",
            "src" =>$this->src,
            "dst" => $this->exten,
            "type" => "incoming",
            "disposition" => "ANSWERED",
            "duration" => $billSec+10,
            "billsec" => $billSec,
            "wait" => 10,
            "unique_id" => $this->uid,
            "cuid" => $this->uid,
            "entry_point" => $this->dst,
            "did" => $this->dst
         ];
        
        return $this->testClass->json('GET', '/api/sea/Cdr', $cdrParams);
    }
}
