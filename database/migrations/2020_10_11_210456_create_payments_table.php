<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->integer("amount");
            $table->integer("credit_id")->nullable();
            $table->string("customer_name")->nullable();
            $table->string("customer_mobile")->nullable();
            $table->string("customer_email")->nullable();
            $table->string("customer_phone_number")->nullable();
            $table->string("driver");
            $table->integer("status");
            $table->dateTime("paid_at")->nullable();
            $table->string("transaction_id");
            $table->string("ref_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
