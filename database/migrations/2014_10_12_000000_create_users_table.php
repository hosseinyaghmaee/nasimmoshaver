<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();

            $table->string("simotel_number")->nullable();
            $table->string('comment')->nullable();
            $table->string("mobile")->nullable();
            $table->string("tell")->nullable();

            $table->integer("level")->default(\App\User::LEVEL_ADVISOR);
            $table->integer("simotel_status")->default(\App\User::SIMOTEL_STATUS_IDLE);
            $table->integer("simotel_paused")->default(false);

            $table->integer("credit_ratio")->nullable();
            $table->integer("entrance_fee")->nullable();

            $table->string('address')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
