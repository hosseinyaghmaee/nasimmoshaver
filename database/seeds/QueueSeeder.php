<?php

use Illuminate\Database\Seeder;

class QueueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Queue::insert(
            [
                [
                    "name" => "مشاوره حقوقی",
                    "credit_ratio" => 1000,
                    "entrance_fee" => 5000,
                    "simotel_number" => "222",
                    "active" => true,
                    "announcement_id"=> 1
                ],
                [
                    "name" => "مشاوره ازدواج",
                    "credit_ratio" => 800,
                    "entrance_fee" => 10000,
                    "simotel_number" => "223",
                    "active" => true,
                    "announcement_id"=> 2
                ],
                [
                    "name" => "مشاور مهاجرت",
                    "credit_ratio" => 500,
                    "entrance_fee" => 5000,
                    "simotel_number" => "224",
                    "active" => true,
                    "announcement_id"=> 3
                ],
                [
                    "name" => "مشاور تحصیلی",
                    "credit_ratio" => 700,
                    "entrance_fee" => 6000,
                    "simotel_number" => "225",
                    "active" => true,
                    "announcement_id"=> 4
                ],
                [
                    "name" => "مشاور سفر",
                    "credit_ratio" => 900,
                    "entrance_fee" => 7000,
                    "simotel_number" => "226",
                    "active" => true,
                    "announcement_id"=> 5

                ]
            ]
        );
    }
}
