<?php

use Illuminate\Database\Seeder;

class CallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<50;$i++){
            $calls= $this->makeRows(500);
            \App\Models\Call::insert($calls);
        }
    }

    private function pollScore()
    {
        return [null, null, null, null, null, null, null, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5][random_int(0, 33)];
    }


    private function makeRows($count)
    {
        $calls = [];
        $callTime = 100;
        $created_at = \Carbon\Carbon::now();
        $creditsCount = \App\Models\Credit::all()->count();
        for ($i = 1; $i <= $count; $i++) {
            $created_at = clone $created_at->subSeconds(random_int(10, 60000));

            $entranceFee = random_int(1, 5) * 1000;
            $creditRatio = random_int(1, 10) * 100;
            $callTime = null;
            $totalAmount = null;
            $poll_score = null;
            $disposition = "NOT ANSWERED";
            $user_id = null;
            $queueId= null;

            if (random_int(1, 5) != 1) {
                $queueId = random_int(1, 5);
            }

            if (random_int(1, 4) != 1) {
                $queueId = random_int(1, 5);
                $callTime = random_int(30, 60*40); //seconds
                $totalAmount = $callTime * ((integer)($creditRatio/60)) + $entranceFee;
                $poll_score = $this->pollScore();
                $disposition = "ANSWERED";
                $user_id = random_int(1, 20);
            }

            $calls[] = [
                "queue_id" => $queueId,
                "user_id" => $user_id,
                "unique_id" => 1,
                "call_time" => $callTime,
                "wait_time" => random_int(5, 30),
                "entrance_fee" => $entranceFee,
                "credit_ratio" => $creditRatio,
                "call_amount" => $callTime * $entranceFee,
                "total_amount" => $totalAmount,
//                "credit_id" => random_int(1,$creditsCount-1),
                "src_number" => "091" . random_int(55555555, 99999999),
                "poll_score" => $poll_score,
                'created_at' => $created_at,
                'disposition' => $disposition,
            ];
        }
        return $calls;
    }
}
