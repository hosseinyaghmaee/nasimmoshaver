<?php

use Illuminate\Database\Seeder;

class CreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\CreditGroup::create([
            "title"=>"خرید های اینترنتی",
            "comment"=>"همه خرید های اینترنتی در این گروه قرار میگیرند",
            "creator_id"=>1,
        ]);

        \App\Models\CreditGroup::create([
            "title"=>"تولید خودکار",
            "comment"=>"کارت های اعتباری تولید شده توسط سیستم تست",
            "creator_id"=>1,
        ]);

        for ($i = 1; $i <= 100; $i++) {
            \App\Models\Credit::createNew(random_int(1, 10) * 100000,null,2);
        }

    }
}
