<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(QueueSeeder::class);
         $this->call(CreditSeeder::class);
        //  $this->call(CallSeeder::class);
         $this->call(ScheduleSeeder::class);
    }
}
