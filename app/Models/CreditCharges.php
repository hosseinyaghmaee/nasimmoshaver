<?php

namespace App\Models;

use App\User;
use Baloot\EloquentHelper;
use Illuminate\Database\Eloquent\Model;

class CreditCharges extends Model
{
    use EloquentHelper;
    protected $fillable=['amount','comment','creator_id'];
    public function credit()
    {
        return $this->belongsTo(Credit::class);
    }

    public function getAmountReadableAttribute()
    {
        return number_format($this->amount);
    }

    public function creator()
    {
        return $this->belongsTo(User::class,"creator_id")->withDefault([
            "name"=>""
        ]);
    }
}

