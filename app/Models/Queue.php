<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    public function activeUsers()
    {
        return $this->belongsToMany(User::class)
            ->where("users.responding_status", "=", User::RESPONDING_STATUS_ACTIVE);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }


    public function scopeLike($query, $field, $value)
    {
        return $query->where($field, 'LIKE', "%$value%");
    }

    public function scopeOrlike($query, $field, $value)
    {
        return $query->orWhere($field, 'LIKE', "%$value%");
    }
}
