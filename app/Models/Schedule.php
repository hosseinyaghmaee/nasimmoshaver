<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{

    public function getDayReadableAttribute()
    {
        return [
            "شنبه",
            "یکشنبه",
            "دوشنبه",
            "سه شنبه",
            "چهار شنبه",
            "پنجشنبه",
            "جمعه",
        ][$this['day'] - 1];
    }

    public function getStartTimeFormattedAttribute()
    {
        $arr = explode(":", $this['start_time']);
        unset($arr[2]);
        return implode(":", $arr);
    }

    public function getEndTimeFormattedAttribute()
    {
        $arr = explode(":", $this['end_time']);
        unset($arr[2]);
        return implode(":", $arr);
    }


    public function scopeGregorianDay($q, $day)
    {
        $persianDay = ($day) % 7 + 2;
        return $q->where("day", $persianDay);
    }


    public function scopeOverlappedWith($q, $startTime, $endTime, $day)
    {
        return $q
            ->where("day", $day)
            ->where(function ($q) use ($startTime, $endTime) {
                return $q
                    ->orWhere(function ($q) use ($startTime, $endTime) {
                        return $q->whereTime("start_time", "<=", $startTime)
                            ->whereTime("end_time", ">=", $startTime);
                    })
                    ->orWhere(function ($q) use ($startTime, $endTime) {
                        return $q->whereTime("start_time", "<=", $endTime)
                            ->whereTime("end_time", ">=", $endTime);
                    })
                    ->orWhere(function ($q) use ($startTime, $endTime) {
                        return $q->whereTime("start_time", ">=", $startTime)
                            ->whereTime("end_time", "<=", $endTime);
                    });
            });
    }

}
