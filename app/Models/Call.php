<?php

namespace App\Models;

use App\User;
use Baloot\EloquentHelper;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use EloquentHelper;


    public function putCredit($credit)
    {
        $this->credit_id = $credit->id;
        $this->save();
    }

    public function queue()
    {
        return $this->belongsTo(Queue::class)->withDefault(['name' => ""]);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault(['name' => ""]);
    }


    public function getCallTimeReadableShortAttribute()
    {
        return $this["call_time"] ? secondsToShortTime($this["call_time"]) : "0";
    }

    public function getCallTimeReadableAttribute()
    {
        return $this["call_time"] ? CarbonInterval::seconds($this['call_time'])->cascade()->forHumans() : "0";
    }
    public function getTotalAmountReadableAttribute()
    {
        return number_format($this["total_amount"]);
    }

    function getHoursMinutes($seconds)
    {

        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds - ($hours * 60 * 60)) / 60);
        $seconds = $seconds - $hours*3600 - $minutes*60;
        return sprintf("%d:%d:%d",
            $hours,
            $minutes,
            $seconds
        );
    }

    public function credit(){
        return $this->belongsTo(Credit::class);
    }

    protected $casts = [
        'create_date' => 'date',
    ];
}
