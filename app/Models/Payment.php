<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_SUCCESSFUL = 1;
    const STATUS_UNSUCCESSFUL = -1;

    public function credit()
    {
        return $this->belongsTo(Credit::class);
    }



    public function setSuccessful($refId)
    {
        $this->ref_id = $refId;
        $this->paid_at = Carbon::now();
        $this->status = \App\Models\Payment::STATUS_SUCCESSFUL;
        $this->save();
    }

    public function setUnsuccessful($refId)
    {
        $this->ref_id = null;
        $this->paid_at = null;
        $this->status = \App\Models\Payment::STATUS_UNSUCCESSFUL;
        $this->save();
    }


}
