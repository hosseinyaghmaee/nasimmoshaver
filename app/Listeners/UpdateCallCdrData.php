<?php

namespace App\Listeners;

use App\Models\Call;
use App\Models\Queue;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class UpdateCallCdrData
{
    /**
    * Handle the event.
    *
    * @param object $event
    * @return void
    */
    public function handle($event)
    {
        $cdrData = collect($event->apiData);

        if (!$cdrData->has('unique_id')) {
            throw new \Exception("unique_id not found");
        }

        $call = Call::whereUniqueId($cdrData->get("unique_id"))->first();

        if (!$call) {
            throw new \Exception("call data not found");
        }


        $queue = Queue::find($call->queue_id);
        if ($queue) {
            $user = User::whereSimotelNumber($cdrData->get('dst'))->first();
            if ($user) {
                $call->user_id = $user->id;
            }
        }

        $call->src_number = $cdrData['src'];

        $entrance_fee = $call->entrance_fee;
        $credit_ratio = $call->credit_ratio;

        $call->disposition = $cdrData->get('disposition');
        if ($cdrData->get('disposition') != "ANSWERED") {
            $call->save();
            return;
        }

        if ($cdrData->has('billsec')) {
            $call->call_time = $cdrData->get('billsec');
            $call->call_amount = (int)($cdrData->get('billsec') / 60 * $credit_ratio);
            $call->total_amount = (int)($call->call_amount + $entrance_fee);
        }
        $call->wait_time =$cdrData->get('wait');
        $call->start_time = $cdrData->get('starttime');
        $call->end_time = $cdrData->get('endtime');
        $call->save();
    }
}
