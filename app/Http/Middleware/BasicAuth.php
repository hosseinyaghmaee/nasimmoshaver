<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = config("simotel.basic-auth.user");
        $pass = config("simotel.basic-auth.pass");

        if ($request->getUser() == $user and $request->getPassword() == $pass)
            return $next($request);

        abort(401);
    }
}
