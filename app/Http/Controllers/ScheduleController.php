<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\MessageBag;

class ScheduleController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $userId = $request->user_id;
        $schedule = $request->isMethod("POST") ? new Schedule : Schedule::findOrFail($request->schedule_id);

        $this->validate($request, $this->validateRequestRules($request));

        $startTime = Carbon::createFromFormat("H:i", $request->start_time)->format("H:i:s");
        $endTime = Carbon::createFromFormat("H:i", $request->end_time)->format("H:i:s");

        $conflictedScheduleTime = Schedule::overlappedWith($startTime, $endTime, $request->day)
            ->whereUserId($userId)->get()->pluck("id")->implode(",");

        if ($conflictedScheduleTime) {
            $bag = (new MessageBag)
                ->add("conflict", $conflictedScheduleTime);

            return redirect()->back()->withErrors($bag)->withInput();
        }

        $schedule->day = $request->day;
        $schedule->start_time = $request->start_time;
        $schedule->end_time = $request->end_time;
        $schedule->user_id = $userId;
        $schedule->save();

        return self::redirectBackWithSuccess("اطلاعات ذخیره شد");
    }


    private function validateRequestRules($request)
    {
        return [
            "user_id" => "required|exists:users,id",
            "day" => "required|in:1,2,3,4,5,6,7",
            "start_time" => "required|date_format:H:i",
            "end_time" => [
                "required", "date_format:H:i",
                function ($attr, $val, $fail) use ($request) {
                    $startTime = Carbon::createFromFormat("H:i", $request->start_time);
                    $endTime = Carbon::createFromFormat("H:i", $request->end_time);
                    $minimumResponseTime = config("nasimMoshaver.schedules.minimumRespondingTime");
                    if ($startTime->greaterThan($endTime->subMinutes($minimumResponseTime)->addMinutes(1)))
                        $fail("بازه زمانی باید حداقل $minimumResponseTime دقیقه باشد");
                }
            ]
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Schedule $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //Authorization
        if (Gate::denies("delete-schedule", $schedule))
            abort(403);

        $schedule->delete();
        return self::redirectBackWithSuccess("انجام شد");
    }
}
