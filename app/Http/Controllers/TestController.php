<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\User;
use Hsy\Simotel\Simotel;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TestController extends Controller
{
    public function users()
    {
        return view("users");
    }

    public function usersData()
    {
        return Datatables::of(User::query()->withCount("queues"))
            ->addIndexColumn()
            ->addColumn('queues_count', function ($row) {
                return $row->queues_count;
            })
            ->rawColumns(['action'])
            ->make(true);
    }


    public function simotel()
    {
        $data = [
            "agents" => [
                ["queue" => "101", "source" => "700", "agent" => "700", "penalty" => ""],
            ]
        ];
        $res = \Simotel::connect()->pbx()->queues()->batchaddagent($data);
        dd($res);
    }

}
