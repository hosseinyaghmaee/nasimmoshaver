<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use Hsy\Options\Options;
use Illuminate\Http\Request;
use Shetabit\Multipay\Exceptions\InvalidPaymentException;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class PurchaseController extends Controller
{

    public function purchase(Request $request)
    {

        if(!$request->has("credit_unique_id"))
            $this->validate($request,
                [
                    "customer_name" => "required",
                    "customer_mobile" => "required|iran_mobile",
                    "customer_email" => "sometimes|nullable|email",
                ]
            );

        $amount = (integer)$request->amount;
        $invoice = (new Invoice)->amount($amount);


        return
            Payment::config($this->paymentConfig())
                ->callbackUrl(route("front.purchase.verify", "zarinpal"))
                ->purchase(
                    $invoice,
                    $this->makePayment($request)
                )
                ->pay()
                ->render();
    }


    public function makePayment($request)
    {
        return function ($driver, $transactionId) use ($request) {

            if ($request->has("credit_unique_id"))
                $credit = Credit::whereUniqueId($request->credit_unique_id)->first();
            else
                $credit = null;


            $payment = new \App\Models\Payment;
            $payment->credit_id = $credit ? $credit->id : null;
            $payment->customer_mobile = $request->customer_mobile;
            $payment->customer_email = $request->customer_email;
            $payment->customer_name = $request->customer_name;
            $payment->customer_phone_number = $request->customer_phone_number;
            $payment->status = \App\Models\Payment::STATUS_PENDING;
            $payment->amount = $request->amount;
            $payment->transaction_id = $transactionId;
            $payment->driver = "zarinpal";
            $payment->save();

        };
    }


    public function verify(Request $request)
    {
        $transactionId = $request->Authority;

        /*
         *  @var $payment Payment
         */
        $payment = \App\Models\Payment::whereTransactionId($transactionId)->first();
        if (!$payment)
            return redirect()->to(route("front.payments.fails"));

        $amount = $payment->amount;

        try {
            $receipt = Payment::config($this->paymentConfig())
            ->amount($amount)->transactionId($transactionId)->verify();

            if ($credit = $payment->credit)
                $credit->chargeCredit($amount);
            else
                $credit = $this->creatCredit($payment);


            $payment->setSuccessful($receipt->getReferenceId());

            return redirect()->to(route("front.credits.show", $credit->unique_id));

        } catch (InvalidPaymentException $exception) {
            /**
             * when payment is not verified, it will throw an exception.
             * We can catch the exception to handle invalid payments.
             * getMessage method, returns a suitable message that can be used in user interface.
             **/


            return redirect()->to(route("front.payments.fails"));

//            echo $exception->getMessage();
        }
    }


    private function creatCredit($payment)
    {
        $credit = Credit::createNew($payment->amount, $payment->customer_phone_number, config("nasimMoshaver.e-creditGroupId"));

        $payment->credit_id = $credit->id;

        return $credit;
    }

    private function paymentConfig()
    {
        return [
            "mode" => \Options::group("site-options")->get("zarinpal-mode") ?: "sandbox",
            "merchantId" => \Options::group("site-options")->get("zarinpal-merchant") ?: "123456789"
        ];
    }


}
