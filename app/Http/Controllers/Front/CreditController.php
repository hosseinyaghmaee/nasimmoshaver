<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Shetabit\Multipay\Invoice;
use Shetabit\Payment\Facade\Payment;

class CreditController extends Controller
{


    public function show($unique_code)
    {
        $credit = \App\Models\Credit::whereUniqueId($unique_code)->first();
        if (!$credit) abort(404);


        return view("front.credits.show", compact("credit"));

    }


    public function updatePhoneNumber(Request $request)
    {
        $credit = Credit::whereUniqueId($request->credit_unique_id)->first();
        if (!$credit) abort(404);

        $credit->phone_number = $request->customer_phone_number;
        $credit->save();

        return self::redirectBackWithSuccess("انجام شد");

    }


}
