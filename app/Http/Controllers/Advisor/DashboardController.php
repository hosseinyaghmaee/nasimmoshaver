<?php

namespace App\Http\Controllers\Advisor;

use App\Classes\Reports\CallsReports;
use App\Http\Controllers\Controller;
use App\Models\Call;
use App\Models\Queue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $report = new CallsReports();
        $report->setRowsPerPage(10);
        $callsReport = (object)$report->statisitcs(null, $user->id);
        $userLastCalls = $report->getCalls(null, $user->id);
        $userQueues = $user->queues()->withCount("users")->get();
        $callsInQueuesReport = Call::select(DB::raw(
            "queues.id, queues.name , sum(calls.call_time) as calls_time_total ,avg(calls.call_time) as calls_time_avg ,avg(calls.poll_score) as poll_score_avg "
        ))
            ->join("queues", "queues.id", "=", "calls.queue_id")->where("calls.user_id", $user->id)
            ->groupBy("queues.id", "queues.name")->get();

        return view("advisor.dashboard.index", compact("callsReport", "callsInQueuesReport", "userQueues", "user","userLastCalls"));

    }
}
