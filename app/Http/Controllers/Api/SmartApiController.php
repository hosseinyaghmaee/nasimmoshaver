<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Nasim\Simotel\Laravel\Facade\Simotel;

class SmartApiController extends Controller
{
    public function call(Request $request)
    {
        try {
            $respond = Simotel::smartApi($request->all())->toArray();
            return response()->json($respond);
        } catch (\Exception $exception) {
            dd($exception->getMessage());
            Log::critical($exception->getMessage());
        }
    }
}
