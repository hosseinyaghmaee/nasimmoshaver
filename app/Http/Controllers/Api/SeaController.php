<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Nasim\Simotel\Laravel\Facade\Simotel;

class SeaController extends Controller
{
    public function dispatchEvent(Request $request, $event)
    {
        try {
            Log::critical($event,$request->all());
            $simotelEventApi = Simotel::eventApi()->dispatch($event,$request->all());

//            $simotelEventApi->dispatchSimotelEvent($request->all());
        } catch (\Exception $exception) {
            Log::critical($exception->getMessage(),$request->all());
        }
    }
}
