<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function index()
    {
        return view("back.options.index");
    }

    public function store(Request $request)
    {

        \Options::group("site-options")->set("site-name", $request["site-name"]);
        \Options::group("site-options")->set("zarinpal-mode", $request["zarinpal-mode"]);
        \Options::group("site-options")->set("zarinpal-merchant", $request["zarinpal-merchant"]);
        \Options::group("site-options")->set("credit-plans", $request["credit_plans"]);
        return self::redirectBackWithSuccess("ذخیره شد");

    }
}
