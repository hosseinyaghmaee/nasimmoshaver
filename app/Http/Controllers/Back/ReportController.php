<?php

namespace App\Http\Controllers\Back;

use App\Classes\Reports\CallsReports;
use App\Http\Controllers\Controller;
use App\Models\Call;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function calls(Request $request)
    {
        $report = new CallsReports;
        return response()->json(
            $report->statisitcs($request->queue_id, $request->user_id, $request->date_from, $request->date_to)
        );
    }
}
