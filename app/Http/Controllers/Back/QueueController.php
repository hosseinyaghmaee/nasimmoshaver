<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Queue;
use App\User;
use Hsy\SimotelConnect\SimotelApi;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class QueueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $queues = Queue::withCount("users")->paginate(100);
        return view("back.queues.all", compact("queues"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $queue = new Queue;
        return view("back.queues.create", compact("queue"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $queue = $request->isMethod("POST") ? new Queue : Queue::findOrFail($request->queue_id);
        $this->validate($request,
            [
                "announcement_id" => "required|integer|unique:queues,announcement_id," . $queue->id,
                "name" => "required",
                "simotel_number" => "sometimes|nullable|unique:queues,simotel_number," . $queue->id,
            ]
        );

        $queue->announcement_id = $request->announcement_id;
        $queue->name = $request->name;
        $queue->simotel_number = $request->simotel_number;
        $queue->entrance_fee = $request->entrance_fee;
        $queue->credit_ratio = $request->credit_ratio;
        $queue->active = true;
        $queue->comment = $request->comment;
        $queue->save();
        return self::redirectWithSuccess(route("admin.queues.index"), "اطلاعات ذخیره شد");

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Queue $queue
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Queue $queue)
    {
        return view("back.queues.show", compact("queue"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Queue $queue
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Queue $queue)
    {
        return view("back.queues.edit", compact("queue"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Queue $queue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Queue $queue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Queue $queue
     * @return \Illuminate\Http\Response
     */
    public function destroy(Queue $queue)
    {
        $queue->users()->detach();
        $queue->delete();
        return self::redirectWithSuccess(route("admin.queues.index"), "حذف شد.");
    }

    /* public function archive(Queue $queue)
     {
         ///soft delete
         $queue->delete();
         return self::redirectWithSuccess(route("admin.queues.archive.index"), "آرشیو شد");
     }*/

    /* public function recycle($user_id)
     {
         $queue = Queue::withTrashed()->findOrFail($user_id);
         $queue->restore();
         return self::redirectWithSuccess(route("admin.queues.show", $queue), "از آرشیو خارج شد");
     }*/

    /*public function archivedList()
    {
        $users = Queue::onlyTrashed()->orderBy("deleted_at", "DESC")->paginate(100);
        return view('back.queues.trash', compact("queues"));
    }*/


    public function addUser(Request $request)
    {

        $this->validate($request, [
            "queue_id" => "required",
            "user_id" => "required",
        ]);

        $user = User::findOrFail($request->user_id);
        $queue = Queue::findOrFail($request->queue_id);


        /* if (config('nasimMoshaver.syncWithSimotel', true)) {
             $simotelApi = new SimotelApi();
             $apiResult = $simotelApi->addToQueue($queue->simotel_number, $user->simotel_number, $user->simotel_number);
             if (!$apiResult) {
                 $message = new MessageBag();
                 $message->add("simotelApi", $simotelApi->getMessage());
                 return redirect()->back()->withErrors($message);
             }
         }*/

        if ($queue->users()->where("users.id", $user->id)->first()) {
            $message = new MessageBag();
            $message->add("user_id", "کاربر انتخاب شده در صف وجود دارد");
            return redirect()->back()->withErrors($message);
        }

        $queue->users()->attach($user);
        return self::redirectBackWithSuccess("انجام شد.");
    }

    /*
        public function changeStatus(Request $request)
        {
            $user = User::findOrFail($request->user_id);
            $queue = Queue::findOrFail($request->queue_id);

            $message = new MessageBag();

            if (config('nasimMoshaver.syncWithSimotel', true)) {
                $simotelApi = new SimotelApi();

                if ($request->active)
                    $apiResult = $simotelApi->resumeInQueue($queue->simotel_number, $user->simotel_number);
                else
                    $apiResult = $simotelApi->pauseInQueue($queue->simotel_number, $user->simotel_number);

                if (!$apiResult)
                    $message->add("simotelApiQueue", $simotelApi->getMessage());
            }

            if ($message->isEmpty())
                $queue->users()->updateExistingPivot($user, ["active" => $request->active]);



            $queue = $queue->users()->whereUserId($user->id)->first();

            return response()->json($queue->pivot);


        }*/

    public function removeUserFromQueue(Request $request)
    {
        $queue = Queue::findOrFail($request->queue_id);
        $user = User::findOrFail($request->user_id);
        if (config('nasimMoshaver.syncWithSimotel', true)) {
            $simotelApi = new SimotelApi();
            $apiResult = $simotelApi->removeFromQueue($queue->simotel_number, $user->simotel_number);
            if (!$apiResult) {
                $message = new MessageBag();
                $message->add("simotelApi", $simotelApi->getMessage());
                return redirect()->back()->withErrors($message);
            }
        }

        $queue->users()->detach($request->user_id);
        return self::redirectBackWithSuccess("از صف خارج شد");
    }

}
