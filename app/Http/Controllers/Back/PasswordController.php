<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    public function form(){
        return view("back.changePassword");
    }

    public function update(Request $request){
        $this->validate($request,[

            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();

        return self::redirectBackWithSuccess("تغییر رمز با موفقیت انجام شد");
    }
}
