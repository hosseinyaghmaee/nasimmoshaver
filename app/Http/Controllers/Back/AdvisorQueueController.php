<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdvisorQueueController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $user = Auth::user();

        $queues =
            $user
                ->queues()
                ->join("calls", "queues.id", "calls.queue_id")
                ->join("calls","users.id","calls.user_id")
        ->get();
        dd($queues);
    }
}
