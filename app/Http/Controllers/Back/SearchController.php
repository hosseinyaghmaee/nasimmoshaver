<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Credit;
use App\Models\Queue;
use App\User;

class SearchController extends Controller
{

    public function search()
    {
        $token = request()->get("q");
        $users = User::like("name", $token)
            ->orLike("mobile", $token)
            ->orLike("simotel_number", $token)
            ->orLike("comment", $token)
            ->orderBy("name", "ASC")
            ->limit(30)->get();


        $queues = Queue::like("name", $token)
            ->orLike("simotel_number", $token)
            ->orLike("comment", $token)
            ->orderBy("name", "ASC")
            ->limit(30)->get();

        $credits = Credit::like("identifier", $token)
            ->orderBy("identifier", "ASC")
            ->limit(30)->get();


        return view("back.search.result", compact('queues', 'users', 'credits'));
    }


    public function searchForUser()
    {
        $term = request('term');
        $users = User
            ::like('name', $term)
            ->orlike('mobile', $term)
            ->orlike('email', $term)
            ->limit(50)
            ->get(['id', 'name',"comment"]);

        return response()->json(compact('users'));
    }
}
