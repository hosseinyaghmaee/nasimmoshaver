<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Call;
use App\Models\Credit;
use App\Models\CreditCharges;
use App\Models\CreditGroup;
use App\Models\CreditGroups;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $credits = Credit
            ::select(
                DB::raw("`credits`.`id`,`identifier`,sum(`credit_charges`.`amount`) as `sum_credit` , sum(`calls`.`total_amount`) as `total_usage` ")
            )
            ->join('credit_charges', 'credits.id', "=", "credit_charges.credit_id")
            ->leftJoin("calls", 'credits.id', '=', 'calls.credit_id')
            ->groupBy("credits.id")
            ->get();
        return view("back.credits.all", compact("credits"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 /*   public function create()
    {
        $credit = new Credit();
        return view("back.credits.create", compact("credit"));
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
 /*   public function store(Request $request)
    {

        $this->validate($request,
            [
                "amount" => "required|numeric",
            ]
        );

       Credit::createNew($request->amount,$request->customer_number);

        return self::redirectWithSuccess(route("admin.credits.index"), "اطلاعات ذخیره شد");
    }*/

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Credit $credit
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Credit $credit)
    {
        $credit->load("charges", "calls", 'calls.user', 'calls.queue');
        return view("back.credits.show", compact('credit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Credit $credit
     * @return \Illuminate\Http\Response
     */
    public function edit(Credit $credit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Credit $credit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Credit $credit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Credit $credit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credit $credit)
    {
        //
    }


    public function addCharge(Request $request, Credit $credit)
    {
        $this->validate($request, [
            'amount' => "required|numeric"
        ]);
        $credit->charges()->create([
            'amount' => $request->amount,
            'comment' => $request->comment,
            'creator_id' => Auth::user() ? Auth::user()->id : null ,
        ]);

        return self::redirectBackWithSuccess("عملیات شارژ انجام شد");
    }


    public function createUniqueUrl(Credit $credit)
    {
        $credit->changeUniqueId();
        return self::redirectBackWithSuccess("لینک منحصر به فرد ایجاد شد.");
    }

}
