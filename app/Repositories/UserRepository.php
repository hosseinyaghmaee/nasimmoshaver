<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 10/19/2020
 * Time: 1:20 PM
 */

namespace App\Repositories;


use App\User;
use Prettus\Repository\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}