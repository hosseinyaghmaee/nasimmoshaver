<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 9/13/2020
 * Time: 12:11 PM
 */

namespace App\Classes\Simotel\SmartApiApps;

use App\User;
use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;

class SelectAdvisor
{
    use SmartApiCommands;
    use MoshavereSmartTrait;

    public function selectAdvisor($appData): array
    {
        $appData=collect($appData);
        if (!$appData->has('data')) {
            $this->cmdExit("wrongExten");
            return $this->okResponse();
        }

        $user = User::whereSimotelNumber($appData->get('data'))->first();
        if (!$user) {
            $this->cmdExit("wrongExten");
            return $this->okResponse();
        }

        $call = $this->getCurrentCall();
        $call->queue_id = null;
        $call->user_id = $user->id;
        $call->entrance_fee = $user->entrance_fee;
        $call->credit_ratio = $user->credit_ratio;
        $call->save();

        $credit = $call->credit;

        $creditTimeMinutes = $this->calculateCreditTime(
            $credit->remain_credit,
            $user->entrance_fee,
            $user->credit_ratio
        );

        $creditTimePeriod = $this->minutesToTimePeriod($creditTimeMinutes);

        $this->cmdPlayAnnouncement("YourCredit");
        $this->cmdSayDuration($creditTimePeriod);
        $this->cmdSetExten($user->simotel_number);
        $this->cmdSetLimitOnCall($creditTimeMinutes * 60);
        $this->cmdExit("toExten");
        return $this->okResponse();
    }
}
