<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 9/13/2020
 * Time: 12:11 PM
 */

namespace App\Classes\Simotel\SmartApiApps;

use App\Models\Call;
use App\Models\Credit;
use App\Models\Queue;
use App\User;
use Illuminate\Support\Facades\Validator;
use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class RegisterCall
{
    use SmartApiCommands;
    use MoshavereSmartTrait;

    public function registerCall($appData): array
    {
        if (!$this->validate()) {
            return $this->errorResponse();
        }
        
        $appData=collect($appData);

        $srcNumber = $appData->get("src");
        
        $credit = $this->getPhoneNumberCredit($srcNumber);

        $call = $this->saveCall($appData,$credit);

        if (!$credit) {
            $this->cmdExit("noCredit");
            return $this->okResponse();
        }

        $creditAmount = $credit->remain_credit;
        $this->cmdPlayAnnouncement("YourCredit");
        $this->cmdSayDigit($creditAmount);
        $this->cmdExit("hasCredit");
        return $this->okResponse();
    }

    private function saveCall($appData,$credit)
    {
        $call = new Call;
        $call->unique_id = $appData->get("unique_id");
        $call->src_number = $appData->get("src");
        $call->dst_number = $appData->get("dst");
        $call->credit_id = $credit ? $credit->id : null;
        $call->save();
    }

    private function getPhoneNumberCredit($phone_number)
    {
        return Credit::wherePhoneNumber($phone_number)->first();
    }

    private function validate()
    {
        $validator = Validator::make(
            request()->all(),
            [
            'unique_id' => 'required|unique:calls,unique_id',
            'dst' => 'required',
            'src' => 'required',
            ]
        );
 
        return !$validator->fails();
    }
}
