<?php
/**
 * Created by PhpStorm.
 * User: 5729906803
 * Date: 9/13/2020
 * Time: 12:11 PM
 */

namespace App\Classes\Simotel\SmartApiApps;

use App\Models\Call;
use App\Models\Queue;
use App\User;
use Hsy\Simotel\SimotelSmartApi\SmartApiCommands;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class RegisterPollScore
{
    use SmartApiCommands;
    use MoshavereSmartTrait;

    public function registerPollScore( $appData): array
    {

        $appData=collect($appData);
        if (!$appData->has('data')) {
            $this->cmdExit("wrongScore");
            return $this->okResponse();
        }

        $scoreData = $appData->get("data");

        if (!in_array($scoreData, $this->pollScores)) {
            $this->cmdExit("wrongScore");
            return $this->okResponse();
        }

        $call = $this->getCurrentCall();
        $call->poll_score = $scoreData;
        $call->save();

        $this->cmdExit("scoreRegistered");

        return $this->okResponse();

    }

   

}
