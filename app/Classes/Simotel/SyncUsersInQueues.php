<?php


namespace App\Classes\Simotel;


use App\Classes\Reports\LoggedInUsers;
use App\Models\Queue;
use App\User;
use Hsy\Options\Facades\Options;
use Illuminate\Support\Facades\Log;

class SyncUsersInQueues
{
    private $log = [];

    private function dump($str)
    {
        $str = is_array($str) ? json_encode($str) : $str;
//        echo "<div style='background-color: #ab154c; margin-bottom: 10px;padding: 6px'><span style='background-color: #aaa; padding: 3px'>$str</span></div>";
    }

    private function log($string)
    {
        $this->log[] = $string;
//        echo "<div>$string</div>";

    }

    public function syncAll()
    {
        \Debugbar::disable();
        $loggedInUsersIds = (new LoggedInUsers)->allUsersIds()->toArray();
        $queues = Queue::with("users")->get();
        $queues = $queues->map(function ($queue) use ($loggedInUsersIds) {
            $users = $queue->users->filter(function ($user) use ($loggedInUsersIds) {
                return in_array($user->id, $loggedInUsersIds);
            })->map(function ($user) {
                return [
                    "id" => $user->id,
                    "simotel_number" => $user->simotel_number,
                ];
            })->toArray();

            return [
                "id" => $queue->id,
                "simotel_number" => $queue->simotel_number,
                "users" => $users,
            ];

        })->toArray();

        $usersInQueues = [];
        foreach ($queues as $queue) {
            foreach ($queue["users"] as $user) {
                $usersInQueues[] = [
                    "queue" => $queue["simotel_number"],
                    "source" => $user["simotel_number"],
                    "agent" => $user["simotel_number"],
                ];
            }
        }

        $cachedUsersInQueues = Options::get("usersInQueues", '');
        Options::set("usersInQueues", json_encode($usersInQueues));

        $cachedUsersInQueues = json_decode($cachedUsersInQueues, true);
        $mustRemove=[];
        foreach ($cachedUsersInQueues as $userInQueue) {
            if (!in_array($userInQueue, $usersInQueues))
                $mustRemove[] = $userInQueue;
        }


        \Simotel::connect()->pbx()->queues()->batchaddagent(["agents" => $usersInQueues]);
        \Simotel::connect()->pbx()->queues()->batchremoveagent(["agents" => $mustRemove]);

        if(!request()->has("cron"))
            dd($usersInQueues,$mustRemove);

    }

    /**
     * @param $user User
     */
    public function pauseUser($user)
    {
        $simotelApi = new SimotelApi();

        $queues = $user->queues;
        $this->dump($queues);
        // all queues
        foreach ($queues as $queue) {
            $res = $simotelApi->pauseInQueue($queue->simotel_number, $user->simotel_number);
            $this->dump($simotelApi->getMessage());
        }

    }

    /**
     * @param $user User
     */
    public function unpauseUser($user)
    {
        $simotelApi = new SimotelApi();

        $queues = $user->queues;

        // all queues
        foreach ($queues as $queue) {
            $res = $simotelApi->resumeInQueue($queue->simotel_number, $user->simotel_number);
            $this->dump($simotelApi->getMessage());
        }
    }
}
