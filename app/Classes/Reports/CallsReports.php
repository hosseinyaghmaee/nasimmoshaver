<?php


namespace App\Classes\Reports;


use App\Models\Call;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;

class CallsReports
{
    private $rowsPerPage = 30;

    public function setRowsPerPage($count)
    {
        $this->rowsPerPage = $count;
    }

    public function statisitcs($queue_id = null, $user_id = null, $date_from = null, $date_to = null, $customQuery = null)
    {
        $query =
            " sum(`total_amount`) as `total_amount` ,sum(`call_time`)  as `call_time` ,count(id) as `calls_count`," .
            " min(`call_time`) as `min_call_time` , max(`call_time`) as `max_call_time`, avg(`poll_score`) as `poll_score_avg`";

        $callsReport = Call::select(DB::raw($query))
            ->when($queue_id, function ($q) use ($queue_id) {
                $q->whereQueueId($queue_id);
            })
            ->when($user_id, function ($q) use ($user_id) {
                $q->whereUserId($user_id);
            })
            ->when($date_from, function ($q) use ($date_from) {
                $q->where("created_at", ">=", $date_from);
            })
            ->when($date_to, function ($q) use ($date_to) {
                $q->where("created_at", "<=", $date_to);
            })
            ->when($customQuery != null, $customQuery)
            ->get()[0];

        $average_call_time = $callsReport->calls_count != 0 ? ((integer)($callsReport->call_time / $callsReport->calls_count)) : 0 ;

        return [
            "call_time" => $callsReport->call_time,
            "call_time_readable" => $callsReport->call_time_readable,
            "call_time_readable_short" => $callsReport->call_time_readable_short,
            "min_call_time" => $callsReport->min_call_time,
            "min_call_time_readable" => CarbonInterval::seconds($callsReport->min_call_time)->cascade()->forHumans(),
            "max_call_time" => $callsReport->max_call_time,
            "max_call_time_readable" => CarbonInterval::seconds($callsReport->max_call_time)->cascade()->forHumans(),
            "total_amount" => number_format($callsReport->total_amount),
            "calls_count" => number_format($callsReport->calls_count),
            "average_call_time" => $average_call_time,
            "average_call_time_readable" => CarbonInterval::seconds($average_call_time)->cascade()->forHumans(),
            "poll_score_avg" => $callsReport->poll_score_avg,
        ];
    }

    public function getCalls($queue_id = null, $user_id = null, $date_from = null, $date_to = null)
    {
        $callsReport = Call::with("queue", 'user')
            ->when($queue_id, function ($q) use ($queue_id) {
                $q->whereQueueId($queue_id);
            })
            ->when($user_id, function ($q) use ($user_id) {
                $q->whereUserId($user_id);
            })
            ->when($date_from, function ($q) use ($date_from) {
                $q->where("created_at", ">=", $date_from);
            })
            ->when($date_to, function ($q) use ($date_to) {
                $q->where("created_at", "<=", $date_to);
            })
            ->orderBy("created_at", "DESC")
            ->paginate($this->rowsPerPage);

        return $callsReport;
    }

    public function usersReport($queue_id = null, $date_from = null, $date_to = null)
    {
        $query =
            " sum(`total_amount`) as `total_amount` ,sum(`call_time`)  as `call_time` ,count(id) as `calls_count`," .
            " min(`call_time`) as `min_call_time` , max(`call_time`) as `max_call_time`, avg(`poll_score`) as `poll_score_avg` , `user_id` ";

        $report = Call::with('user')
            ->select(DB::raw($query))
            ->when($queue_id, function ($q) use ($queue_id) {
                $q->whereQueueId($queue_id);
            })
            ->when($date_from, function ($q) use ($date_from) {
                $q->where("created_at", ">=", $date_from);
            })
            ->when($date_to, function ($q) use ($date_to) {
                $q->where("created_at", "<=", $date_to);
            })
            ->groupBy('user_id')
            ->orderBy("total_amount", "DESC")
            ->paginate($this->rowsPerPage);

        return $report;
    }
}
