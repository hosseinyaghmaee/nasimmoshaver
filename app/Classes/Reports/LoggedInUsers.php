<?php


namespace App\Classes\Reports;

use App\Models\Schedule;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class LoggedInUsers
{

    private $day;
    private $startTime;
    private $endTime;


    /**
     * @return Collection
     */
    public function allUsersIds()
    {
        return $this->allUsers()->pluck("user_id");
    }

    /**
     * @return Collection
     */
    public function allUsers()
    {
        $day = $this->day ? $this->day : $this->jalaliToday();
        $startTime = $this->startTime ? $this->startTime : Carbon::now()->format("H:i:s");
        $endTime = $this->endTime ? $this->endTime : Carbon::now()->format("H:i:s");

        return Schedule::overlappedWith($startTime, $endTime, $day)
            ->get("user_id");

    }


    public function users(){
        return User::whereIn("id",$this->allUsersIds())->withCount("queues")->get();
    }

    /**
     * @param mixed $day
     */
    public function setDay($day): void
    {
        $this->day = $day;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime): void
    {
        $this->endTime = $endTime;
    }

    private function jalaliToday()
    {
        $days = [
            0=>2,
            1=>3,
            2=>4,
            3=>5,
            4=>6,
            5=>7,
            6=>1,
        ];
        return $days[Carbon::now()->dayOfWeek];
    }
}
