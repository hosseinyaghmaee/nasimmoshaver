<?php


namespace App\Classes\Reports;


use App\Models\Call;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Support\Facades\DB;

class CallsAnalyze
{
    private $userId;
    private $queueId;
    private $since;
    private $to;
    private $customQuery;
    private $fields = [];

    public function __construct()
    {
        $this->addFields(
            array(
                DB::raw("sum(`total_amount`) as `total_amount`"),
                DB::raw("sum(`call_time`)  as `call_time`"),
                DB::raw("count(id) as `calls_count`"),
                DB::raw("min(`call_time`) as `min_call_time`"),
                DB::raw("max(`call_time`) as `max_call_time`"),
                DB::raw("avg(`poll_score`) as `poll_score_avg`"),
                DB::raw("avg(`call_time`) as `average_call_time`"),
            )
        );
    }


    public function all()
    {
        return $this->get();
    }

    public function first()
    {
        return $this->get()->first();
    }

    private function get()
    {
        if ($this->getFields())
            return $this->query()->get($this->fields);

        return $this->query()->get();
    }

    private function query()
    {

        $userId = $this->userId;
        $queueId = $this->queueId;
        $since = $this->since;
        $to = $this->to;
        $customQuery = $this->customQuery;

        return Call::when($queueId, function ($q) use ($queueId) {
            $q->whereQueueId($queueId);
        })
            ->when($userId, function ($q) use ($userId) {
                $q->whereUserId($userId);
            })
            ->when($since, function ($q) use ($since) {
                $q->where("created_at", ">=", $since);
            })
            ->when($to, function ($q) use ($to) {
                $q->where("created_at", "<=", $to);
            })
            ->when($customQuery !== null, $customQuery);

    }

    private function getFields()
    {
        return $this->fields;
    }

    public function lastDaysReport($days = 30)
    {
        return $this->setSince(Carbon::now()->subDays($days)->format("Y-m-d 00:00:00"))
            ->addFields([DB::raw("Date(`calls`.`created_at`) as `create_date` ")])
            ->setCustomQuery(function ($q) {
                $q->groupBy("create_date");
            })->all();
    }

    /**
     * @param mixed $queueId
     * @return CallsAnalyze
     */
    public function setQueueId($queueId): CallsAnalyze
    {
        $this->queueId = $queueId;
        return $this;
    }

    /**
     * @param mixed $since
     * @return CallsAnalyze
     */
    public function setSince($since): CallsAnalyze
    {
        $this->since = $since;
        return $this;
    }

    /**
     * @param mixed $to
     * @return CallsAnalyze
     */
    public function setTo($to): CallsAnalyze
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @param mixed $userId
     * @return CallsAnalyze
     */
    public function setUserId($userId): CallsAnalyze
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param mixed $customQuery
     * @return CallsAnalyze
     */
    public function setCustomQuery($customQuery): CallsAnalyze
    {
        $this->customQuery = $customQuery;
        return $this;
    }

    /**
     * @param mixed $fields
     * @return CallsAnalyze
     */
    public function addFields($fields): CallsAnalyze
    {
        $this->fields = array_merge($this->fields, $fields);
        return $this;
    }
}
