<?php
/**
 * Created by PhpStorm.
 * User: HosseinYaghmaee@gmail.com
 * Date: 9/13/2020
 * Time: 12:57 PM
 */

return [

    /*
     *
     */
    "smartApi" => [
        "methodsRepositoryClass" => \App\Classes\Simotel\SmartApiMethodsRepo::class,
    ],

    /*
     *
     */
    "simotelApi" => [
        "apiUrl" => env("SIMOTEL_API_SERVER", "http://37.156.144.147/api/v1/"),
        "user" => env("SIMOTEL_API_USER", "user"),
        "pass" => env("SIMOTEL_API_PASS", "pass"),
    ],

    "basic-auth" => [
        "user" => env("SIMOTEL_BASIC_AUTH_USER", "admin"),
        "pass" => env("SIMOTEL_BASIC_AUTH_PASS", "admin"),
    ],

    /*
     *
     */
    "simotelEventApi" => [
        "events" => [
            "Cdr" => \Hsy\SimotelConnect\Events\SimotelEventCdr::class,
            "NewState" => \Hsy\SimotelConnect\Events\SimotelEventNewState::class,
            "ExtenAdded" => \Hsy\SimotelConnect\Events\SimotelEventExtenAdded::class,
            "ExtenRemoved" => \Hsy\SimotelConnect\Events\SimotelEventExtenRemoved::class,
            "IncomingCall" => \Hsy\SimotelConnect\Events\SimotelEventIncomingCall::class,
            "OutGoingCall" => \Hsy\SimotelConnect\Events\SimotelEventOutgoingCall::class,
            "Transfer" => \Hsy\SimotelConnect\Events\SimotelEventTransfer::class,
        ]
    ]
];
