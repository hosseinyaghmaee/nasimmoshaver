<?php

use Illuminate\Support\Facades\Route;

Route::namespace("\App\Http\Controllers\Front")->as("front.")
    ->group(function () {
        Route::get("/", "HomeController@index")->name("home.index");

        Route::post("/purchase", "PurchaseController@purchase")->name("purchase");
        Route::get("/purchase/verify/{driver}", "PurchaseController@verify")->name("purchase.verify");

        Route::get("/credits/{unique_id}", "CreditController@show")->name("credits.show");
        Route::post("/credits/updatePhoneNumber", "CreditController@updatePhoneNumber")->name("credits.updatePhoneNumber");

        Route::get("/payments/fails", "PaymentController@fails")->name("payments.fails");

        Route::get("syncQueues", function () {
            $sync = new \App\Classes\Simotel\SyncUsersInQueues();
            $sync->syncAll();
        });

    });

Auth::routes(["register" => false]);

Route::any("/clear", function () {
    \Illuminate\Support\Facades\Artisan::call("view:clear");
});
Route::any("/migrate/fresh", function () {
    \Illuminate\Support\Facades\Artisan::call("migrate:fresh");
});

Route::get("shetabit/start", "ShetabitTestController@start");
Route::get("shetabit/verify", "ShetabitTestController@verify")->name("shetabit.verify");

Route::get("users/test", "TestController@users");
Route::get("users/data", "TestController@usersData")->name("users.data");

Route::get("fields", function () {
    $class = new \App\Classes\Simotel\SimotelSettings();
    dd($class->getGroupedFields());
});

Route::get("firstuser", function () {
    $user = \App\User::find(1);
    $user->level = \App\User::LEVEL_ADMIN;
    $user->password = bcrypt("asd123ASD");
    $user->save();
});

Route::get("createuseradmin", function () {
    if(\App\User::count()>0)
        die("exists");

    \App\User::create([
        'name' => 'username',
        'email' => request("email"),
        'password' => bcrypt("asd123ASD"),
        'simotel_number' => "",
        'mobile' => "",
        'comment' => "",
        'address' => "تهران، آدرس مربوط به محل کاربر ادمین",
        'level' =>  \App\User::LEVEL_ADMIN,
        'entrance_fee' => "1000",
        'credit_ratio' => "1000",
    ]);

});

Route::get("logtest", function () {
    \Illuminate\Support\Facades\Log::info("test");
});


Route::get("ternary", function () {
    return view("ternary");
});


Route::get("/test/simotel","TestController@simotel");
